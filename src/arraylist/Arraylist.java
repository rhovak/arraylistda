/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraylist;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author TPSI
 */
public class Arraylist {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int id;
        String nome;
        int posicao;
        mostrarmenu();
        // TODO code application logic here
        ArrayList<String> nomeilhas = new ArrayList<>();
        ArrayList<Integer> ilhas = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        Scanner sci = new Scanner(System.in);
        int menu;
        do {
            menu = Integer.parseInt(sc.nextLine());
            if (menu == 1) {
                System.out.print("Diz o numero da ilha: ");
                id = sci.nextInt();
                if (ilhas.contains(id)) {
                    System.out.print("essa ilha ja existe: \n");
                    mostrarmenu();
                } else {

                    ilhas.add(id);
                    System.out.print("Diz o nome da ilha: ");
                    nomeilhas.add(sc.nextLine());
                    mostrarmenu();
                }

            }
            if (menu == 2) {
                System.out.println("Indique o ID da ilha que quer Editar");
                id = sc.nextInt();

                sc.nextLine();
                posicao = ilhas.indexOf(id);
                if (posicao == -1) {
                    System.out.println("Esse Id nao existe");
                    break;
                }
                System.out.println("Indique o novo nome");
                nome = sc.nextLine();
                nomeilhas.set(posicao, nome);
                System.out.println("Editado com sucesso");
                mostrarmenu();
            }
            if (menu == 3) {
                System.out.println("Indique o ID da ilha que quer remover");
                id = sc.nextInt();
                sc.nextLine();
                posicao = ilhas.indexOf(id);

                ilhas.remove(posicao);
                nomeilhas.remove(posicao);
                ilhas.trimToSize();
                nomeilhas.trimToSize();
                System.out.println("Removido com sucesso");
                mostrarmenu();
            }
            if (menu == 4) {
                System.out.println("Indique o numero  da ilha que quer procurar");
                id = sci.nextInt();
                for (int i = 0; i < ilhas.size(); i++) {
                    if (ilhas.get(i).equals(id)) {
                        System.out.println("* ------|---------");
                        System.out.println("* " + ilhas.get(i) + "\t| " + nomeilhas.get(i));

                    }
                }
                mostrarmenu();
            }
            if (menu == 5) {
                System.out.println("Indique o nome  da ilha que quer procurar");
                String s = sc.nextLine();
                for (int i = 0; i < ilhas.size(); i++) {
                    if (nomeilhas.get(i).contains(s)) {
                        System.out.println("* ------|---------");
                        System.out.println("* " + ilhas.get(i) + "\t| " + nomeilhas.get(i));

                    }
                }

                mostrarmenu();
            }

            if (menu == 6) {

                for (int i = 0; i < ilhas.size(); i++) {
                    System.out.println("* ------|---------");
                    System.out.println("* " + ilhas.get(i) + "\t| " + nomeilhas.get(i));

                }

                mostrarmenu();
            }
            if (menu == 9) {
                mostrarmenu();
            }

        } while (menu != 0);

    }

    private static void mostrarmenu() {
        System.out.println("***********************************************");
        System.out.println("** M E N U                                   **");
        System.out.println("** 1- Inserir Ilhas                          **");
        System.out.println("** 2- Modificar Ilhas                        **");
        System.out.println("** 3- Eliminar Ilhas                         **");
        System.out.println("** 4- Encontrar Ilha por número              **");
        System.out.println("** 5- Encontrar Ilha por nome                **");
        System.out.println("** 6- Listar Ilhas                           **");
        System.out.println("** 9- Mostrar Menu                           **");
        System.out.println("** 0- Sair                                   **");
        System.out.println("***********************************************");

    }
}
